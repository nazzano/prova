module.exports = {
    devtool: 'source-map',
    entry: {
        main: './src/app.js',
        lazio: './src/lazio.js'
    },
    mode: 'development',
    module: {
        rules: [{
            exclude: /node_modules/,
            test: /\.js$/
        }]
    },
    output: {
        filename: '[name].bundle.js',
        path: __dirname + '/dist'
    }
}